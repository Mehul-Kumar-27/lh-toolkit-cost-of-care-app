import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cost_of_care/models/download_cdm_model.dart';
import 'package:cost_of_care/repository/download_cdm_repository_impl.dart';

import 'bloc.dart';

class DownloadCdmBloc extends Bloc<DownloadCdmEvent, DownloadCdmState> {
  DownloadCDMRepositoryImpl downloadCDMRepositoryImpl;

  DownloadCdmBloc(this.downloadCDMRepositoryImpl) : super(LoadingState());

  List<DownloadCdmModel> hospitals = [];

  DownloadCdmState get initialState => LoadingState();

  @override
  Stream<DownloadCdmState> mapEventToState(
    DownloadCdmEvent event,
  ) async* {
    if (event is DownloadCDMFetchData) {
      yield LoadingState();
      if (await downloadCDMRepositoryImpl.checkDataSaved(event.stateName)) {
        hospitals =
            await downloadCDMRepositoryImpl.getSavedData(event.stateName);
        yield LoadedState(hospitals);
      } else {
        try {
          List<dynamic> response =
              await downloadCDMRepositoryImpl.fetchData(event.stateName);
          hospitals = await downloadCDMRepositoryImpl.parseData(response);
          // print("index 0 hospital list" + hospitals[0].hospitalAddress);
          yield LoadedState(hospitals);
          downloadCDMRepositoryImpl.saveData(hospitals, event.stateName);
        } catch (e) {
          yield ErrorState(e.message);
        }
      }
    } else if (event is AddToBookmark) {
      //hospitals[event.index].isDownload = 1;
      yield (AddedToBookmarkState(event.index));
      hospitals[event.index].isBookmarked = 1;
      List<DownloadCdmModel> copyList =
          await downloadCDMRepositoryImpl.getSavedData(event.stateName);
      for (int i = 0; i < copyList.length; i++) {
        if (copyList[event.index].hospitalName ==
            hospitals[event.index].hospitalName) {
          copyList[event.index].isBookmarked = 1;
          // copyList[event.index].bookmarked = 1;
          break;
        }
      }
      downloadCDMRepositoryImpl.saveData(copyList, event.stateName);
      yield RefreshedState(hospitals);
    } else if (event is RemoveToBookmark) {
      //hospitals[event.index].isDownload = 1;
      yield (RemovedToBookmarkState(event.index));

      hospitals[event.index].isBookmarked = 0;
      List<DownloadCdmModel> copyList =
          await downloadCDMRepositoryImpl.getSavedData(event.stateName);
      for (int i = 0; i < copyList.length; i++) {
        if (copyList[event.index].hospitalName ==
            hospitals[event.index].hospitalName) {
          copyList[event.index].isBookmarked = 0;
          // copyList[event.index].bookmarked = 1;
          break;
        }
      }
      downloadCDMRepositoryImpl.saveData(copyList, event.stateName);
      yield RefreshedState(hospitals);
    } else if (event is DownloadCDMRefreshList) {
      hospitals[event.index].isDownload = 1;
      //  hospitals[event.index].bookmarked = 1;
      List<DownloadCdmModel> copyList =
          await downloadCDMRepositoryImpl.getSavedData(event.stateName);
      for (int i = 0; i < copyList.length; i++) {
        if (copyList[event.index].hospitalName ==
            hospitals[event.index].hospitalName) {
          copyList[event.index].isDownload = 1;
          // copyList[event.index].bookmarked = 1;
          break;
        }
      }
      downloadCDMRepositoryImpl.saveData(copyList, event.stateName);
      yield RefreshedState(hospitals);
    } else if (event is DownloadCDMSearchHospital) {
      List<DownloadCdmModel> copyList =
          await downloadCDMRepositoryImpl.getSavedData(event.stateName);
      hospitals = [];
      copyList.forEach((element) {
        if (element.hospitalName.toLowerCase().contains(event.query) ||
            element.hospitalAddress.toLowerCase().contains(event.query)) {
          hospitals.add(element);
          // print("query address" + element.hospitalAddress);
        }
      });
      yield RefreshedState(hospitals);
    } else if (event is DownloadCDMError) {
      yield ErrorState(event.message);
    }
  }
}
