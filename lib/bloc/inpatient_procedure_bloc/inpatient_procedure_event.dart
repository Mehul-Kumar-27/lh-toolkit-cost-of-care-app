part of 'inpatient_procedure_bloc.dart';

abstract class InpatientProcedureEvent extends Equatable {
  const InpatientProcedureEvent();
}

class FetchData extends InpatientProcedureEvent {
  final bool isHardRefresh;

  FetchData({isHardRefresh}) : isHardRefresh = isHardRefresh ?? false;

  @override
  List<Object> get props => [];
}
